package com.AccountSystem.proj.SalaryCode.domain.repository;

import com.AccountSystem.proj.SalaryCode.domain.model.SalaryCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryCodeRepository extends JpaRepository<SalaryCode,Long> {
}
