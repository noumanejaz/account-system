package com.AccountSystem.proj.Employee.domain.repository;

import com.AccountSystem.proj.Employee.domain.model.Job;
import com.AccountSystem.proj.Employee.domain.model.LeaveCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LeaveCodeRepository extends JpaRepository<LeaveCode,Long>{

    @Query(value = "Select count(id) from leave_code",nativeQuery = true)
    public Long getLeaveCodeCount();
}
