package com.AccountSystem.proj.Employee.domain.repository;

import com.AccountSystem.proj.Employee.domain.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    @Query(value = "Select count(id) from employee;",nativeQuery = true)
    public long getEmployeeCount();
}
