package com.AccountSystem.proj.Employee.domain.repository;

import com.AccountSystem.proj.Employee.domain.model.Allowance;
import com.AccountSystem.proj.Employee.domain.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobRepository extends JpaRepository<Job,Long> {

    public Job findJobByJobCode(String jobCode);

    @Query(value = "Select count(id) from job", nativeQuery = true)
    public Long getJobCodeCount();
}
