package com.AccountSystem.proj.Employee.domain.repository;

import com.AccountSystem.proj.Employee.domain.model.Allowance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AllowanceRepository extends JpaRepository<Allowance,Long> {

    @Query(value = "select * from allowance where salary_codes_id=:id",nativeQuery = true)
    List<Allowance> getBYSalaryID(Long id);
}
