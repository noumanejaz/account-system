package com.AccountSystem.proj.Employee.controller;


import com.AccountSystem.proj.Commons.ApiResponse;
import com.AccountSystem.proj.Employee.domain.service.LeaveCodeService;
import com.AccountSystem.proj.Employee.dto.LeaveCodeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/leavecode")
public class LeaveCodeController {

    @Autowired
    LeaveCodeService leaveCodeService;

    @PostMapping("/post")
    public ApiResponse postLeaveCode(@RequestBody LeaveCodeDTO leaveCodeDTO){
        return leaveCodeService.postLeaveCode(leaveCodeDTO);
    }

    @GetMapping("/")
    public ApiResponse getAllLeaveCode(){
        return leaveCodeService.getAllLeaveCode();
    }

    @GetMapping("/{id}")
    public ApiResponse getLeaveCodeById(@PathVariable("id") Long id){
        return leaveCodeService.getLeaveCodeById(id);
    }

    @DeleteMapping("/")
    public ApiResponse deleteAll(){
        return leaveCodeService.deleteAll();
    }

    @DeleteMapping("/{id}")
    public ApiResponse deleteLeaveCodeById(@PathVariable("id") Long id){
        return leaveCodeService.deleteLeaveCodeById(id);
    }

    @PutMapping("/{id}")
    public ApiResponse updateLeaveCode(@PathVariable("id") Long id, @RequestBody LeaveCodeDTO leaveCodeDTO){
        return leaveCodeService.updateLeaveCode(id,leaveCodeDTO);
    }

}
