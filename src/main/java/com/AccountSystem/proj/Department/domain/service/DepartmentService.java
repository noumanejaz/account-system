package com.AccountSystem.proj.Department.domain.service;

import com.AccountSystem.proj.Commons.ApiResponse;
import com.AccountSystem.proj.Department.dto.DepartmentDTO;

import java.util.List;

public interface DepartmentService {
    ApiResponse getAllDepartments();

    ApiResponse  createDepartment(DepartmentDTO departmentDTO);

    ApiResponse  getDepartmentById(Long id);
}
