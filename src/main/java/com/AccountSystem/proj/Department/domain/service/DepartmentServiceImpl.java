 package com.AccountSystem.proj.Department.domain.service;

 import com.AccountSystem.proj.Department.domain.model.Department;
 import com.AccountSystem.proj.Department.domain.repository.DepartmentRepository;
 import com.AccountSystem.proj.Department.dto.DepartmentDTO;
 import org.springframework.beans.factory.annotation.Autowired;
 import com.AccountSystem.proj.Commons.ApiResponse;
 import org.springframework.stereotype.Service;

 import java.time.LocalDateTime;
 import java.util.List;
@Service
 public class DepartmentServiceImpl implements DepartmentService {
     @Autowired
     DepartmentRepository departmentRepository;


     @Override
     public ApiResponse getAllDepartments() {
         return new ApiResponse(200,"successfully fetched data",departmentRepository.findAll());
     }

     @Override
     public ApiResponse  createDepartment(DepartmentDTO departmentDTO) {
         Department department = new Department();
         department.setDescription(departmentDTO.getDescription());
         department.setTitle(departmentDTO.getTitle());


         try{
             return new ApiResponse(200,"successfully Added",departmentRepository.save(department));
         }
         catch (Exception e)
         {
             return new ApiResponse(500,"Some Error Occured",null);
         }
     }

     @Override
     public ApiResponse  getDepartmentById(Long id) {
         try
         {
             return new ApiResponse(200,"successfully fetched data",departmentRepository.findById(id));
         }
         catch (Exception e)
         {
             return new ApiResponse(500,"Some Error Occured",null);
         }
     }
 }
