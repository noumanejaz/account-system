package com.AccountSystem.proj.Department.domain.repository;

import com.AccountSystem.proj.Department.domain.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department,Long> {

}
