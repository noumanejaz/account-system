package com.AccountSystem.proj.Department.controller;

import com.AccountSystem.proj.Commons.ApiResponse;
import com.AccountSystem.proj.Department.domain.service.DepartmentService;
import com.AccountSystem.proj.Department.dto.DepartmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/department", produces = MediaType.APPLICATION_JSON_VALUE)
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @GetMapping("/")
    public ApiResponse getAllDepartments() {


        return departmentService.getAllDepartments();
    }

    @GetMapping("/{id}")
    public ApiResponse getAllDepartments(@RequestParam("id") Long id ) {


        return departmentService.getDepartmentById(id);
    }

    @PostMapping
    public ApiResponse createDepartment(@RequestBody DepartmentDTO departmentDTO) {

        return departmentService.createDepartment(departmentDTO);
    }
}
