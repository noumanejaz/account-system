package com.AccountSystem.proj.Department.dto;

public class DepartmentDTO {
    public DepartmentDTO(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public DepartmentDTO() {
    }

    private  String title;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
